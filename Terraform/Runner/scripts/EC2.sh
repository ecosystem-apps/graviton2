#! /bin/bash

cd /root
apt-get update
apt-get upgrade -yq
apt-get install rand 



export token=${runner_token}
export url=${runner_url}
export token2=$runner_token
export token3=`echo ${runner_token}`



  # -- update the runner toml ----
echo "post registration" >> /root/debug.txt
env >> /root/debug.txt

sudo cat > /etc/gitlab-runner/config.toml << EOF

concurrent = 4

[[runners]]
  name = "gitlab-ec2-docker-runner-`date +%Y%m%d-%H%M%S`-`rand`"
  url = "`echo ${runner_url}`"
  token = "`echo ${runner_token}`"
  executor = "docker"
  limit = 10


  [runners.docker]
    privileged = true
    tls_verify = false
    disable_cache = true
    image = "alpine:latest"
  [runners.cache]
    Type = "s3"
    [runners.cache.s3]
      ServerAddress = "`curl http://169.254.169.254/latest/meta-data/local-ipv4`:9005"
 #     AccessKey = `cat /export/.minio.sys/config/config.json | grep accessKey | awk '{print $2}' | sed 's/.$//'`
      AccessKey= "minioadmin"
      SecretKey= "minioadmin"
 #     SecretKey = `cat /export/.minio.sys/config/config.json | grep secretKey | awk '{print $2}' | sed 's/.$//'`
      BucketName = "runner"
      Insecure = true

EOF

# -- register the runner --- 

sudo gitlab-runner register \
  --non-interactive \
  --url $url \
  --registration-token $token \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "ARM64-docker-runner-`date +%Y%m%d-%H%M%S`-`rand`" \
  --tag-list "graviton2,arm64,aws,docker" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"




