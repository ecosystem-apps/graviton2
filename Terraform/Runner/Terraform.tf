
terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "alliances-graviton2"
    key    = "graviton2-Runner"
    region = "us-east-1"
    encrypt = true
    }
  }
#note ^^^ key hardcoded. Checking logic

