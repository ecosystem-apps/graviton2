resource "random_string" "zone" {
  length           = 7
  upper            = false
  lower            = true
  number           = true
  special          = false
  override_special = "/@\" "
}
