resource "aws_security_group" "Arm_SSH" {
  name = "${var.SG-Prefix}ssh-${random_string.zone.result}"
  description = "ICMP & SSH CONNECTIONS INBOUND (managed by Terraform)"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
