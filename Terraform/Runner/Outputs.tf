output "Runner-Public-IP-Address" {
  value = "${aws_instance.Runner.public_ip}"
}

output "token-used-1" {
  value = var.runner_token
}

output "Gitlab-URL" {
  value = var.runner_url
}