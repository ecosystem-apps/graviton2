data "aws_ami" "Preview-AMI" {
  most_recent = true
  owners = ["871730672732"]
  filter {
      name   = "name"
      values = ["KH-GitLab-Runner-Docker-ARM64-*"]
      }
  }

data "template_file" "init" {
  template = file("${path.module}/scripts/EC2.sh")
  vars = {
    runner_token = var.runner_token
    runner_url = var.runner_url
  }
}

resource "aws_instance" "Runner" {
  ami = data.aws_ami.Preview-AMI.id
  instance_type = var.EC2_instance_type
  vpc_security_group_ids = [
    "${aws_security_group.Arm_SSH.id}"
  ]
  key_name = var.ssh_key
  user_data = data.template_file.init.rendered
  tags = {
    Name = "Runner-${random_string.zone.result}"
    }
}
