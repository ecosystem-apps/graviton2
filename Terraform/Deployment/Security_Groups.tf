resource "aws_security_group" "ICMP_HTTP_HTTPS_SSH_3000" {
  name = "${var.SG-Prefix}icmp_http_https_ssh_3000-${random_string.zone.result}"
  description = "ICMP, 3000, HTTP, HTTPS & SSH CONNECTIONS INBOUND (managed by Terraform)"

  ingress {
         from_port = 443
         to_port = 443
         protocol = "TCP"
         cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
       from_port = 80
       to_port = 80
       protocol = "TCP"
       cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
       from_port = 3000
       to_port = 3000
       protocol = "TCP"
       cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
    lifecycle {
    create_before_destroy = true
    }
}
