# Find the Route53 zone ID
data "aws_route53_zone" "public" {
  name         = var.Domain
  private_zone = false
}

resource "aws_route53_record" "Preview-pub" {
  zone_id = data.aws_route53_zone.public.zone_id
  name    = "${var.Host_Name}${random_string.zone.result}.${var.Domain}"

  type = "A"
  ttl  = var.ttl
  records = [
    aws_instance.Preview.public_ip,
  ]
}
