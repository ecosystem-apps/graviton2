output "Preview-URL" {
  value = "http://${var.Host_Name}${random_string.zone.result}.${var.Domain}:3000"
}

output "Public-IP-Address" {
  value = "${aws_instance.Preview.public_ip}"
}

output "branch_used" {
  value = var.GitLab_Branch
}