variable "region" {
  default     = "us-east-1"
  description = "The name of the AWS region you'd like to deploy in."
}
# Using the default VPC in the selected region for this demo.
data "aws_vpc" "default" {
  default = true
}

variable "ssh_key" {
  # default = "kelly-mbp"
  default     = "cs-demo"
  description = "This is the name of your provisioning machine's public SSH key"
}

variable "EC2_instance_type" {
  default = "m6g.medium"
}

variable "SG-Prefix" {
  default     = "KH-Graviton2-"
  description = "Prefix to add to security groups"
}

variable "ttl" {
  default     = 30
  description = "Default DNS time to live for a record. Variable set to 30 seconds"
}

variable "private_ssh_key_dir" {
  default     = "/Users/kelly/.ssh/cs-demo.pem"
  description = "Location of the PRIVATE key provisioning infrastructure."
}

variable "Domain" {
  default = "gl-demo.io"
}

variable "Host_Name" {
  default = "preview."
}

variable "GitLab_Branch" {
  description = "The name of the branch to checkout out on deployment. This variable is passed by GitLab CI"
  default = "master"
}