data "aws_ami" "Preview-AMI" {
  most_recent = true
  owners = ["099720109477"]
  filter {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-arm64*"]
      }
      }

resource "aws_instance" "Preview" {
  ami = data.aws_ami.Preview-AMI.id
  instance_type = var.EC2_instance_type
  vpc_security_group_ids = [
    "${aws_security_group.ICMP_HTTP_HTTPS_SSH_3000.id}"
  ]
  key_name = var.ssh_key
  tags = {
    Name = "Preview-${random_string.zone.result}"
    }
  user_data = <<-EOT
    #! /bin/bash
    cd /root
    git clone https://gitlab.com/gitlab-com/alliances/aws/sandbox-projects/graviton2
    cd graviton2
    git checkout "${var.GitLab_Branch}"
    # git rev-parse --abbrev-ref HEAD> /root/branch.txt
    export DEBIAN_FRONTEND=noninteractive
    sudo apt-get update
    sudo apt-get upgrade -yq
    # Install Node 12.6
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    sudo apt-get install -y nodejs make g++ libsqlcipher-dev libsqlite3-dev libsqlite3-dev cmdtest
    npm install sqlite3 --build-from-source --sqlite_libname=sqlcipher --sqlite=/usr/local --verbose
    npm install 
    npm start
EOT 
}