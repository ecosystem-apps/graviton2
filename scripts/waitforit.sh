#! /bin/bash
while :
do
	ready=`docker ps | grep [minio,registry] | wc -l`
    echo $ready
	if [ $ready -eq 2 ]
	then
		break
	fi
	loops=$((loops+1))
	echo $loops
    if [ $loops -eq 1000 ]
    then 
        break
    fi
done

