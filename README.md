# Graviton2

Sample Graviton2 project.

What does this project do?
* Optionally sets up and registers a dedicated ARM64 runner to run the tagged jobs
* Up to Two Terraform workspaces - one for the GitLab runner and another to Build a Deployment/Test server. Deployment is set to always run; Runner is optional as noted above.
* Complete pipeline runs tests against a node.js project & deploys it to EC2 along with a deployment URL.

## Running locally
* TODO - workspace creation -update this

## CI/CD settings

Variable settings:

* AWS_ACCESS_KEY_ID - AWS access key (set to *Masked*)
* AWS_SECRET_ACCESS_KEY - AWS secret (set to *Masked*)
* RUNNER_DEPLOY - If present then GitLab CI will bring up a dedicated ARM64 Runner to run `arm64` tagged jobs
* TF_VAR_runner_token - Grab from Settings - CI/CD - Runners - Set up a specific Runner manually
* TF_VAR_runner_url - Grab from Settings - CI/CD - Runners - Set up a specific Runner manually.  Include HTTP/HTTPS & last /

## Related projects

* [AWS AMI Packer builder project.](https://gitlab.com/khair1/packer-ami-builder) This project builds an ARM64 based Docker image & then builds an ARM64 based Runner from this image. The resulting image is used in the Runner scripts in this project. 
* [Minio ARM64 container project.](https://gitlab.com/gitlab-com/alliances/aws/sandbox-projects/minio-arm64) Simple project to build & publish a usable Minio container. The community only compiles for AMD64 so this is needed for the Runner cache

## Acknowledgements

* [Node.js code](https://github.com/sitepoint-editors/notes-board). Code was discovered in  Sitepoint's "[9 Practical Node.js Projects](https://www.sitepoint.com/premium/books/9-practical-node-js-projects)"  Code from this project was released with an MIT license.

=========

Original README from Sitepoint project: 

# How to Build and Structure a Node.js MVC Application

In this tutorial, James Kolce shows how to create a note-taking app using Hapi.js, Pug, Sequelize and SQLite. Learn to build Node.js MVC apps by example.

https://www.sitepoint.com/node-js-mvc-application

## Requirements

* [Node.js](http://nodejs.org/) (v8 or higher)

## Installation Steps

1. Clone repo
2. Run `npm install`
3. Run `npm run start:dev`
4. Visit http://localhost:3000/

## License

The MIT License (MIT)

Copyright (c) 2020 SitePoint

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.